var namespace_u_b_x_1_1_cfg =
[
    [ "Q_prt", "class_u_b_x_1_1_cfg_1_1_q__prt.html", "class_u_b_x_1_1_cfg_1_1_q__prt" ],
    [ "Prt", "class_u_b_x_1_1_cfg_1_1_prt.html", "class_u_b_x_1_1_cfg_1_1_prt" ],
    [ "Q_msg", "class_u_b_x_1_1_cfg_1_1_q__msg.html", "class_u_b_x_1_1_cfg_1_1_q__msg" ],
    [ "Msg", "class_u_b_x_1_1_cfg_1_1_msg.html", "class_u_b_x_1_1_cfg_1_1_msg" ],
    [ "Q_GNSS", "class_u_b_x_1_1_cfg_1_1_q___g_n_s_s.html", "class_u_b_x_1_1_cfg_1_1_q___g_n_s_s" ],
    [ "GNSS_config", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config" ],
    [ "GNSS", "class_u_b_x_1_1_cfg_1_1_g_n_s_s.html", "class_u_b_x_1_1_cfg_1_1_g_n_s_s" ],
    [ "Mode_Charlen", "namespace_u_b_x_1_1_cfg.html#aa9a28b16d34f6d5f8064a89320f17459", [
      [ "Bits_5", "namespace_u_b_x_1_1_cfg.html#aa9a28b16d34f6d5f8064a89320f17459aa74e6a87b98dc872dbefb5288d8d5e7e", null ],
      [ "Bits_6", "namespace_u_b_x_1_1_cfg.html#aa9a28b16d34f6d5f8064a89320f17459a863f4093e5acf3df80ace9e73fa932ad", null ],
      [ "Bits_7", "namespace_u_b_x_1_1_cfg.html#aa9a28b16d34f6d5f8064a89320f17459a9dd6dbebbbaed8e0c755cf9e4d0eefa6", null ],
      [ "Bits_8", "namespace_u_b_x_1_1_cfg.html#aa9a28b16d34f6d5f8064a89320f17459a8fd0be36e80dc0e579793d57047c3f6a", null ]
    ] ],
    [ "Mode_Parity", "namespace_u_b_x_1_1_cfg.html#a384740b4d3cc6a892d5e80986e97dd17", [
      [ "Even", "namespace_u_b_x_1_1_cfg.html#a384740b4d3cc6a892d5e80986e97dd17a35537fbc25d87ffe59e4f35fefcd34b7", null ],
      [ "Odd", "namespace_u_b_x_1_1_cfg.html#a384740b4d3cc6a892d5e80986e97dd17a37b6bd7fe61d651735cec3d3b0356c66", null ],
      [ "Reserved", "namespace_u_b_x_1_1_cfg.html#a384740b4d3cc6a892d5e80986e97dd17a942d4e37dd5607ab68e54755540d4a47", null ],
      [ "None", "namespace_u_b_x_1_1_cfg.html#a384740b4d3cc6a892d5e80986e97dd17a6adf97f83acf6453d4a6a4b1070f3754", null ]
    ] ],
    [ "Mode_Stopbits", "namespace_u_b_x_1_1_cfg.html#a600180667e1ed7355a008e6276db66bc", [
      [ "Bits_1", "namespace_u_b_x_1_1_cfg.html#a600180667e1ed7355a008e6276db66bca06658291886e6b1cd0c5aac8dcd61235", null ],
      [ "Bits_1_5", "namespace_u_b_x_1_1_cfg.html#a600180667e1ed7355a008e6276db66bcaa9541cbb4b742b045e989fae578548e1", null ],
      [ "Bits_2", "namespace_u_b_x_1_1_cfg.html#a600180667e1ed7355a008e6276db66bca48e5e9fa22723cadb3c09d09917d5ee3", null ],
      [ "Bits_0_5", "namespace_u_b_x_1_1_cfg.html#a600180667e1ed7355a008e6276db66bcac6665ef32195e7cb1470425bb2a3cb1f", null ]
    ] ],
    [ "sigCfgMask", "namespace_u_b_x_1_1_cfg.html#a789bf41255d68ed96e8502e3b4adb308", [
      [ "GPS_L1CA", "namespace_u_b_x_1_1_cfg.html#a789bf41255d68ed96e8502e3b4adb308aae7fec9803d586a45c9c3ac836d0e36f", null ],
      [ "SBAS_L1CA", "namespace_u_b_x_1_1_cfg.html#a789bf41255d68ed96e8502e3b4adb308a84fc64c5884df5b626cf9889cd0c71b4", null ],
      [ "BDS_B1I", "namespace_u_b_x_1_1_cfg.html#a789bf41255d68ed96e8502e3b4adb308ae3ff0fdcd65945a310af25ac87d9143f", null ],
      [ "QZSS_L1CA", "namespace_u_b_x_1_1_cfg.html#a789bf41255d68ed96e8502e3b4adb308a1e76723677fc6c9cb70b514ea4cd2b1b", null ],
      [ "QZSS_L1SAIF", "namespace_u_b_x_1_1_cfg.html#a789bf41255d68ed96e8502e3b4adb308a5fe6e196004c70b66d9a05721d8b1301", null ],
      [ "GLONASS_L1OF", "namespace_u_b_x_1_1_cfg.html#a789bf41255d68ed96e8502e3b4adb308a4b7b5d992b450c0feffbbdc2cf5669d0", null ]
    ] ],
    [ "txReady_polarity", "namespace_u_b_x_1_1_cfg.html#a93b749c8ccd97d20c977ef7e3b4a84ea", [
      [ "Hi_active", "namespace_u_b_x_1_1_cfg.html#a93b749c8ccd97d20c977ef7e3b4a84eaada085683b6e211d22c26db5c7079870e", null ],
      [ "Lo_active", "namespace_u_b_x_1_1_cfg.html#a93b749c8ccd97d20c977ef7e3b4a84eaafb002e7f129b8b344e1a7b1b192b5d7e", null ]
    ] ]
];