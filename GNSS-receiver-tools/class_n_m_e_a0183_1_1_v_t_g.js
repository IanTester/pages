var class_n_m_e_a0183_1_1_v_t_g =
[
    [ "ptr", "class_n_m_e_a0183_1_1_v_t_g.html#a7caed6524d53a1e979f41bd66572f65a", null ],
    [ "VTG", "class_n_m_e_a0183_1_1_v_t_g.html#a0276c1e6d127110930509d98bdae1aa1", null ],
    [ "magnetic_course", "class_n_m_e_a0183_1_1_v_t_g.html#ae0d1818f057e1b0732fac0eec2061ceb", null ],
    [ "receiver_mode", "class_n_m_e_a0183_1_1_v_t_g.html#a28d88b540e1093d63a5b2e7a63b5ceb5", null ],
    [ "speed", "class_n_m_e_a0183_1_1_v_t_g.html#a9a4787e978b3e3e73623ad7a2eaac135", null ],
    [ "speed_knots", "class_n_m_e_a0183_1_1_v_t_g.html#a3a89c3ae66424cae0661f89d8dc73598", null ],
    [ "true_course", "class_n_m_e_a0183_1_1_v_t_g.html#a2419a3332832f3a1af4effb6038ba106", null ]
];