var class_n_m_e_a0183_1_1_r_m_c =
[
    [ "ptr", "class_n_m_e_a0183_1_1_r_m_c.html#a5a97722300bb79395f49aa113a929147", null ],
    [ "RMC", "class_n_m_e_a0183_1_1_r_m_c.html#a48679cdcd2bbf5327b03f2c727479846", null ],
    [ "course", "class_n_m_e_a0183_1_1_r_m_c.html#a2dbe0c857803e1a91f4905829f6a87c4", null ],
    [ "lattitude", "class_n_m_e_a0183_1_1_r_m_c.html#ae4915671623d1fc61c48d579ae230b4c", null ],
    [ "longitude", "class_n_m_e_a0183_1_1_r_m_c.html#a41181bc72defddd83ae2081084f64dca", null ],
    [ "receiver_mode", "class_n_m_e_a0183_1_1_r_m_c.html#a897bfee2df2d2058b09e273dcaec2fa9", null ],
    [ "speed", "class_n_m_e_a0183_1_1_r_m_c.html#af2ab77971234d76902709965ed5691d5", null ],
    [ "status", "class_n_m_e_a0183_1_1_r_m_c.html#aab3c8231f4058107661970b948a5272d", null ],
    [ "UTC_date", "class_n_m_e_a0183_1_1_r_m_c.html#a9974658afc8761dda9ca10685ec9423c", null ],
    [ "UTC_datetime", "class_n_m_e_a0183_1_1_r_m_c.html#a4f81150a08ad24c9aa053f95de91f5cb", null ],
    [ "UTC_time", "class_n_m_e_a0183_1_1_r_m_c.html#ae92b0f0bc4180be19d3837464609ba18", null ]
];