var class_g_p_s_1_1_sat__clock__and__health =
[
    [ "Sat_clock_and_health", "class_g_p_s_1_1_sat__clock__and__health.html#afd3ad4927e0bcba11fb80b120a0fe6b0", null ],
    [ "a_f0", "class_g_p_s_1_1_sat__clock__and__health.html#a1d218d62616ca7dcb5aa2a7e7cee28a2", null ],
    [ "a_f0_raw", "class_g_p_s_1_1_sat__clock__and__health.html#a473b882a52dd612f8511286094508049", null ],
    [ "a_f1", "class_g_p_s_1_1_sat__clock__and__health.html#abb4c5848395ee9bd76b0365fa81a4713", null ],
    [ "a_f1_raw", "class_g_p_s_1_1_sat__clock__and__health.html#a55d099c41b3b9e3ca2dd4870ecf46762", null ],
    [ "a_f2", "class_g_p_s_1_1_sat__clock__and__health.html#aa0dac336253c4808f2120b86f8093008", null ],
    [ "a_f2_raw", "class_g_p_s_1_1_sat__clock__and__health.html#aace6573a8db2a50fafd08e89fac07840", null ],
    [ "health", "class_g_p_s_1_1_sat__clock__and__health.html#aec9e94becd6146bbf7ce93f3bc7dc343", null ],
    [ "IODC", "class_g_p_s_1_1_sat__clock__and__health.html#aac67a18f7c11cc585094119cead521a5", null ],
    [ "navigation_data_ok", "class_g_p_s_1_1_sat__clock__and__health.html#ac4655a14b034f93c3c01c90d5d4d4258", null ],
    [ "T_GD", "class_g_p_s_1_1_sat__clock__and__health.html#a55f24012defdde113472e92203f0e09e", null ],
    [ "T_GD_raw", "class_g_p_s_1_1_sat__clock__and__health.html#ac5f3de3e5ded78b4bc994622bfda825c", null ],
    [ "t_OC", "class_g_p_s_1_1_sat__clock__and__health.html#a8dfae0d8c42983a2b40565876256f058", null ],
    [ "t_OC_raw", "class_g_p_s_1_1_sat__clock__and__health.html#af84d3bcbd8df467bf1fcc8f28593c1d7", null ],
    [ "URA", "class_g_p_s_1_1_sat__clock__and__health.html#ad82acb8ce67329a7fac1bc83b3c0081a", null ],
    [ "week_number", "class_g_p_s_1_1_sat__clock__and__health.html#a83f1b2fcc46ad35ab1c0cfdbc94f18bc", null ]
];