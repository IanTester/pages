var class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status =
[
    [ "GNSS_pos_pinning_status", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html#a93899b0b9faeb32fd66184db94b15014", null ],
    [ "pinning_count", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html#adaa67ed5cbe55ee4975e59206fa332af", null ],
    [ "pinning_speed", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html#a9869d6e0b23d9c41f425250674517eee", null ],
    [ "status", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html#af18799d5ad38e827bcae6f5bb06721b3", null ],
    [ "unpinning_count", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html#a06e4da229e3422c806f6c1811f5576b3", null ],
    [ "unpinning_distance", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html#a5fc881c364931d4e3bd3bf3c1a46ea52", null ],
    [ "unpinning_speed", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html#ab5a2cb88af9227f6dd07285e2befa504", null ]
];