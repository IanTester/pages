var namespace_n_m_e_a0183 =
[
    [ "Skytraq", "namespace_n_m_e_a0183_1_1_skytraq.html", "namespace_n_m_e_a0183_1_1_skytraq" ],
    [ "InvalidSentence", "class_n_m_e_a0183_1_1_invalid_sentence.html", "class_n_m_e_a0183_1_1_invalid_sentence" ],
    [ "ChecksumMismatch", "class_n_m_e_a0183_1_1_checksum_mismatch.html", "class_n_m_e_a0183_1_1_checksum_mismatch" ],
    [ "UnknownSentenceType", "class_n_m_e_a0183_1_1_unknown_sentence_type.html", "class_n_m_e_a0183_1_1_unknown_sentence_type" ],
    [ "Sentence", "class_n_m_e_a0183_1_1_sentence.html", "class_n_m_e_a0183_1_1_sentence" ],
    [ "GGA", "class_n_m_e_a0183_1_1_g_g_a.html", "class_n_m_e_a0183_1_1_g_g_a" ],
    [ "GLL", "class_n_m_e_a0183_1_1_g_l_l.html", "class_n_m_e_a0183_1_1_g_l_l" ],
    [ "GSA", "class_n_m_e_a0183_1_1_g_s_a.html", "class_n_m_e_a0183_1_1_g_s_a" ],
    [ "SatelliteData", "struct_n_m_e_a0183_1_1_satellite_data.html", "struct_n_m_e_a0183_1_1_satellite_data" ],
    [ "GSV", "class_n_m_e_a0183_1_1_g_s_v.html", "class_n_m_e_a0183_1_1_g_s_v" ],
    [ "RMC", "class_n_m_e_a0183_1_1_r_m_c.html", "class_n_m_e_a0183_1_1_r_m_c" ],
    [ "VTG", "class_n_m_e_a0183_1_1_v_t_g.html", "class_n_m_e_a0183_1_1_v_t_g" ],
    [ "ZDA", "class_n_m_e_a0183_1_1_z_d_a.html", "class_n_m_e_a0183_1_1_z_d_a" ],
    [ "FixQuality", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46ae", [
      [ "Unavailable", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aea453e6aa38d87b28ccae545967c53004f", null ],
      [ "SPSmode", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aeaf96ca3891a20024d07a64bc83adddcc6", null ],
      [ "DGPSmode", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aea13bf92a5337fb3cf05a8e43e87429e9a", null ],
      [ "PPSmode", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aea157f8da1fa96e0399786eaa146eedbfb", null ],
      [ "RTKmode", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aea3694c12a0681ee774565e93ece82dacc", null ],
      [ "FloatRTKmode", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aea1366b6617e2ee29cf96a9e7820c6fe43", null ],
      [ "DeadReckoningMode", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aeafe945d60087f38c20724bb73e43f953f", null ],
      [ "ManualMode", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aea35fe396889ec1365d72a2300a9e1c59b", null ],
      [ "SimulationMode", "namespace_n_m_e_a0183.html#ae7b81b2e62b4cbcc6561a054217b46aea3f1dd6a979372c3c631f3aaa558dda9d", null ]
    ] ],
    [ "FixType", "namespace_n_m_e_a0183.html#ab433b019f1fdc66200387d281c3010cd", [
      [ "NotAvailable", "namespace_n_m_e_a0183.html#ab433b019f1fdc66200387d281c3010cda534ceac854da4ba59c4dc41b7ab732dc", null ],
      [ "TwoDimensional", "namespace_n_m_e_a0183.html#ab433b019f1fdc66200387d281c3010cda5c439358dbee64daba17b27a722983ff", null ],
      [ "ThreeDimensional", "namespace_n_m_e_a0183.html#ab433b019f1fdc66200387d281c3010cdae53e6a468bc80e076b0d8c3f17f7251e", null ]
    ] ],
    [ "OpMode", "namespace_n_m_e_a0183.html#ad4add89b28c75830e7e0205e3c9275e1", [
      [ "Manual", "namespace_n_m_e_a0183.html#ad4add89b28c75830e7e0205e3c9275e1ae1ba155a9f2e8c3be94020eef32a0301", null ],
      [ "Automatic", "namespace_n_m_e_a0183.html#ad4add89b28c75830e7e0205e3c9275e1a086247a9b57fde6eefee2a0c4752242d", null ]
    ] ],
    [ "ReceiverMode", "namespace_n_m_e_a0183.html#a927c0bad84f71ff8e857aa93c167d8b6", [
      [ "unknown", "namespace_n_m_e_a0183.html#a927c0bad84f71ff8e857aa93c167d8b6aad921d60486366258809553a3db49a4a", null ],
      [ "NotValid", "namespace_n_m_e_a0183.html#a927c0bad84f71ff8e857aa93c167d8b6a04665ec171e86ef749cc563d7bdeec91", null ],
      [ "Autonomous", "namespace_n_m_e_a0183.html#a927c0bad84f71ff8e857aa93c167d8b6a6aec1991f208e2948db5e4eee6e1ccff", null ],
      [ "Differential", "namespace_n_m_e_a0183.html#a927c0bad84f71ff8e857aa93c167d8b6a1c2eaa5d56340808f2a94368501e5496", null ],
      [ "Estimated", "namespace_n_m_e_a0183.html#a927c0bad84f71ff8e857aa93c167d8b6a3c311fbd0f9e51ce27b984f55164cf83", null ],
      [ "Simulated", "namespace_n_m_e_a0183.html#a927c0bad84f71ff8e857aa93c167d8b6a82abd462f92fed638828a450a2ba1f2a", null ]
    ] ],
    [ "dm_to_degrees", "namespace_n_m_e_a0183.html#a2f73fed36de5ac6ab4edc38e32eeaeab", null ],
    [ "generate_checksum", "namespace_n_m_e_a0183.html#a14bb7c1d54c56056c1e39b7cb88f898e", null ],
    [ "hhmmss_to_duration", "namespace_n_m_e_a0183.html#aaf156d73a7c76de1c18aa22043dd3483", null ],
    [ "parse_sentence", "namespace_n_m_e_a0183.html#a13e6d7d6fc95ea1ff212304ed1074a61", null ],
    [ "read_receivermode", "namespace_n_m_e_a0183.html#a0c205f58bd9746e0f97be12585ac6581", null ],
    [ "split_fields", "namespace_n_m_e_a0183.html#a0cdcb19beab093730cd7b9b4a7e38c12", null ]
];