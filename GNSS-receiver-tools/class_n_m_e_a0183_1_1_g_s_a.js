var class_n_m_e_a0183_1_1_g_s_a =
[
    [ "ptr", "class_n_m_e_a0183_1_1_g_s_a.html#ac3fa9c7002e3bc266edce610ed953089", null ],
    [ "GSA", "class_n_m_e_a0183_1_1_g_s_a.html#a8ad91106d9b37424a154443dd6fe3472", null ],
    [ "fix_type", "class_n_m_e_a0183_1_1_g_s_a.html#a51b48c21772fbe2396a3b06db66bfbe7", null ],
    [ "HDOP", "class_n_m_e_a0183_1_1_g_s_a.html#a25aef5e328c25bfdc895865d02dcc7d9", null ],
    [ "mode", "class_n_m_e_a0183_1_1_g_s_a.html#aa6e08785b89c3173a5a8a5b2edc0b13a", null ],
    [ "PDOP", "class_n_m_e_a0183_1_1_g_s_a.html#a637d8271ba608352cd89e2411dcf0a7f", null ],
    [ "satellite_ids", "class_n_m_e_a0183_1_1_g_s_a.html#aa629248028b35fee23f680d1ec8a8595", null ],
    [ "VDOP", "class_n_m_e_a0183_1_1_g_s_a.html#a85661ed577c58018d060497225e44f53", null ]
];