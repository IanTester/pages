var searchData=
[
  ['signal_5fconfig_5fmask_1975',['signal_config_mask',['../struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html#afb77f287db7cf78c8a4f07ff384f745f',1,'UBX::Cfg::GNSS_config']]],
  ['snr_1976',['snr',['../struct_n_m_e_a0183_1_1_satellite_data.html#acd9851c68c6dc17855cc0b9ff41e66fe',1,'NMEA0183::SatelliteData']]],
  ['startseq_5flen_1977',['StartSeq_len',['../namespace_sky_traq_bin.html#a4967ad167360fc1aefbcb853b07dd66a',1,'SkyTraqBin']]],
  ['sv_5fhas_5falmanac_1978',['sv_has_almanac',['../struct_sky_traq_bin_1_1_sv_status.html#a036960cab7b0bda82ed4d09fcea8be6d',1,'SkyTraqBin::SvStatus']]],
  ['sv_5fhas_5fephemeris_1979',['sv_has_ephemeris',['../struct_sky_traq_bin_1_1_sv_status.html#a598af2a77dc61b96393f5985a7653923',1,'SkyTraqBin::SvStatus']]],
  ['sv_5fis_5fhealthy_1980',['sv_is_healthy',['../struct_sky_traq_bin_1_1_sv_status.html#adc53495caa10eb5bdc4e2c5ddd30b0df',1,'SkyTraqBin::SvStatus']]],
  ['syncchar_1981',['SyncChar',['../namespace_u_b_x.html#ac4414020ad7051bedf5722a21e16e7ae',1,'UBX']]],
  ['syncchar_5flen_1982',['SyncChar_len',['../namespace_u_b_x.html#a86af1e7a55c806ccb6e2150e88ca1c0e',1,'UBX']]]
];
