var namespace_u_b_x =
[
    [ "Ack", "namespace_u_b_x_1_1_ack.html", "namespace_u_b_x_1_1_ack" ],
    [ "Cfg", "namespace_u_b_x_1_1_cfg.html", "namespace_u_b_x_1_1_cfg" ],
    [ "ChecksumMismatch", "class_u_b_x_1_1_checksum_mismatch.html", "class_u_b_x_1_1_checksum_mismatch" ],
    [ "UnknownMessageID", "class_u_b_x_1_1_unknown_message_i_d.html", "class_u_b_x_1_1_unknown_message_i_d" ],
    [ "Message", "class_u_b_x_1_1_message.html", "class_u_b_x_1_1_message" ],
    [ "Output_message", "class_u_b_x_1_1_output__message.html", "class_u_b_x_1_1_output__message" ],
    [ "Input_message", "class_u_b_x_1_1_input__message.html", "class_u_b_x_1_1_input__message" ],
    [ "Input_Output_message", "class_u_b_x_1_1_input___output__message.html", "class_u_b_x_1_1_input___output__message" ],
    [ "checksum_t", "struct_u_b_x_1_1checksum__t.html", "struct_u_b_x_1_1checksum__t" ],
    [ "Length", "namespace_u_b_x.html#a4918ace5c6520675590439090c86cad0", null ],
    [ "output_message_factory", "namespace_u_b_x.html#a3387892a2abd388ede48be4a0663af25", null ],
    [ "Class_ID", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcf", [
      [ "NAV", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfaa681762fc2fd8fa38ed9a79e359d1e19", null ],
      [ "RXM", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfaed2f9471b22e4b445fe99d72c2d57c5c", null ],
      [ "INF", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfa9517fd0bf8faa655990a4dffe358e13e", null ],
      [ "ACK", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfa0fc437bc317835cad5faafc12a83fad5", null ],
      [ "CFG", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfafdd77d900c9de5478ef8d20aff4680dc", null ],
      [ "UPD", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfa73b316aaf5f3c3a6be46e9beb87fbf03", null ],
      [ "MON", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfab8a17e8439000d1794cd35a7793e0824", null ],
      [ "AID", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfa5ef612cfef9d854de7c3f963fcc33c8b", null ],
      [ "TIM", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfadcdef55fae0dff158393fd0c5c455dd0", null ],
      [ "MGA", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfae4e393f29af8aac8d614354c3d5f3f4b", null ],
      [ "LOG", "namespace_u_b_x.html#ae465ddaf6a89cf6d972466c00132adcfa4b5ffcdaf38ce4d463171f5c977c5ab3", null ]
    ] ],
    [ "GNSS_ID", "namespace_u_b_x.html#a7681883beadf5a754d66eb9afe563ec1", [
      [ "GPS", "namespace_u_b_x.html#a7681883beadf5a754d66eb9afe563ec1a8c578de37278ada488d763ea86c5cf20", null ],
      [ "SBAS", "namespace_u_b_x.html#a7681883beadf5a754d66eb9afe563ec1a34dd33959e729910d676a3108e81ac7f", null ],
      [ "Galileo", "namespace_u_b_x.html#a7681883beadf5a754d66eb9afe563ec1a380ece00e3a5c3b9c876cbeb0c8a38bb", null ],
      [ "BeiDou", "namespace_u_b_x.html#a7681883beadf5a754d66eb9afe563ec1a532e1e7daeef2562426638f7092cf9a6", null ],
      [ "IMES", "namespace_u_b_x.html#a7681883beadf5a754d66eb9afe563ec1ae19daf4d2998f9b8d0afc4fb5af0e125", null ],
      [ "QZSS", "namespace_u_b_x.html#a7681883beadf5a754d66eb9afe563ec1a10b5ac7cdca8f06ca82238f8f3b2d119", null ],
      [ "GLONASS", "namespace_u_b_x.html#a7681883beadf5a754d66eb9afe563ec1a48549df6f41418cb94b963fe84894cd6", null ]
    ] ],
    [ "PortID", "namespace_u_b_x.html#aa73a5edee0441ba1ca65f8c31a965c8e", [
      [ "DDC", "namespace_u_b_x.html#aa73a5edee0441ba1ca65f8c31a965c8ea34ccd3ae5d03f22bd20e6792834a2702", null ],
      [ "I2C", "namespace_u_b_x.html#aa73a5edee0441ba1ca65f8c31a965c8eafb87bf1fc88386ca21395991f90fff95", null ],
      [ "UART", "namespace_u_b_x.html#aa73a5edee0441ba1ca65f8c31a965c8eacec5769b01fb096efaf0d6186823c78f", null ],
      [ "USB", "namespace_u_b_x.html#aa73a5edee0441ba1ca65f8c31a965c8ea7aca5ec618f7317328dcd7014cf9bdcf", null ],
      [ "SPI", "namespace_u_b_x.html#aa73a5edee0441ba1ca65f8c31a965c8ea33dc5312b091968f5a120c2484d40df8", null ]
    ] ],
    [ "checksum", "namespace_u_b_x.html#a8b0c644ba0dcefea6f8a45daf739ed06", null ],
    [ "parse_message", "namespace_u_b_x.html#a6f78883edb71f26b4e9edd6a2537f5fd", null ],
    [ "Checksum_len", "namespace_u_b_x.html#ac4affcec67fd6cee3a3043147f0ec483", null ],
    [ "ClassID_len", "namespace_u_b_x.html#affc72c0b2db51ca08effb9b176871936", null ],
    [ "Length_len", "namespace_u_b_x.html#a34a723029c1d38dc0fe28ae5db75195b", null ],
    [ "output_message_factories", "namespace_u_b_x.html#a436ec0ef70a431f39d1478f47ba04d2f", null ],
    [ "SyncChar", "namespace_u_b_x.html#ac4414020ad7051bedf5722a21e16e7ae", null ],
    [ "SyncChar_len", "namespace_u_b_x.html#a86af1e7a55c806ccb6e2150e88ca1c0e", null ]
];