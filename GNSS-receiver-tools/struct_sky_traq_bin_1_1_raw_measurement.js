var struct_sky_traq_bin_1_1_raw_measurement =
[
    [ "RawMeasurement", "struct_sky_traq_bin_1_1_raw_measurement.html#a95019652f4c4b06c226cd85dc1585309", null ],
    [ "carrier_phase", "struct_sky_traq_bin_1_1_raw_measurement.html#af3567913629bbd26d5572e3317dcaac9", null ],
    [ "CN0", "struct_sky_traq_bin_1_1_raw_measurement.html#a357062908f98fb1d04d930234b460589", null ],
    [ "coherent_integration_time", "struct_sky_traq_bin_1_1_raw_measurement.html#ace65fd4f010f1026c1fad0d3f080fc30", null ],
    [ "doppler_freq", "struct_sky_traq_bin_1_1_raw_measurement.html#a43cea4b0a3ef30b918f853abae4d41b7", null ],
    [ "has_carrier_phase", "struct_sky_traq_bin_1_1_raw_measurement.html#aaf7d694afe9ddad439b0135fd9db8597", null ],
    [ "has_doppler_freq", "struct_sky_traq_bin_1_1_raw_measurement.html#a316df0b947920f3ee4a39e6afcf6be44", null ],
    [ "has_pseudo_range", "struct_sky_traq_bin_1_1_raw_measurement.html#ab8cd4453e4297a05826465e53d8e14a6", null ],
    [ "PRN", "struct_sky_traq_bin_1_1_raw_measurement.html#a15fd5bc0532ba364ea571804315f3587", null ],
    [ "pseudo_range", "struct_sky_traq_bin_1_1_raw_measurement.html#a9edb6d693161801674f8e9a2bb217ad3", null ],
    [ "will_cycle_slip", "struct_sky_traq_bin_1_1_raw_measurement.html#acde2df4c887a496c23fde780ad3f5237", null ]
];