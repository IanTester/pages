var files_dup =
[
    [ "BE.cc", "_b_e_8cc.html", "_b_e_8cc" ],
    [ "BE.hh", "_b_e_8hh.html", "_b_e_8hh" ],
    [ "bitstream.cc", "bitstream_8cc.html", "bitstream_8cc" ],
    [ "bitstream.hh", "bitstream_8hh.html", "bitstream_8hh" ],
    [ "GNSS.hh", "_g_n_s_s_8hh.html", [
      [ "Message", "class_g_n_s_s_1_1_message.html", "class_g_n_s_s_1_1_message" ],
      [ "InsufficientData", "class_g_n_s_s_1_1_insufficient_data.html", "class_g_n_s_s_1_1_insufficient_data" ],
      [ "InvalidMessage", "class_g_n_s_s_1_1_invalid_message.html", "class_g_n_s_s_1_1_invalid_message" ]
    ] ],
    [ "GPSNav.cc", "_g_p_s_nav_8cc.html", "_g_p_s_nav_8cc" ],
    [ "GPSNav.hh", "_g_p_s_nav_8hh.html", "_g_p_s_nav_8hh" ],
    [ "LE.cc", "_l_e_8cc.html", "_l_e_8cc" ],
    [ "LE.hh", "_l_e_8hh.html", "_l_e_8hh" ],
    [ "NMEA-0183.cc", "_n_m_e_a-0183_8cc.html", "_n_m_e_a-0183_8cc" ],
    [ "NMEA-0183.hh", "_n_m_e_a-0183_8hh.html", "_n_m_e_a-0183_8hh" ],
    [ "Parser.cc", "_parser_8cc.html", "_parser_8cc" ],
    [ "Parser.hh", "_parser_8hh.html", "_parser_8hh" ],
    [ "SkyTraq.cc", "_sky_traq_8cc.html", "_sky_traq_8cc" ],
    [ "SkyTraq.hh", "_sky_traq_8hh.html", "_sky_traq_8hh" ],
    [ "SkyTraqBin.cc", "_sky_traq_bin_8cc.html", "_sky_traq_bin_8cc" ],
    [ "SkyTraqBin.hh", "_sky_traq_bin_8hh.html", "_sky_traq_bin_8hh" ],
    [ "SkyTraqBin_inputs.cc", "_sky_traq_bin__inputs_8cc.html", null ],
    [ "SkyTraqBin_inputs.hh", "_sky_traq_bin__inputs_8hh.html", [
      [ "Restart_sys", "class_sky_traq_bin_1_1_restart__sys.html", "class_sky_traq_bin_1_1_restart__sys" ],
      [ "Q_sw_ver", "class_sky_traq_bin_1_1_q__sw__ver.html", "class_sky_traq_bin_1_1_q__sw__ver" ],
      [ "Q_sw_CRC", "class_sky_traq_bin_1_1_q__sw___c_r_c.html", "class_sky_traq_bin_1_1_q__sw___c_r_c" ],
      [ "Set_factory_defaults", "class_sky_traq_bin_1_1_set__factory__defaults.html", "class_sky_traq_bin_1_1_set__factory__defaults" ],
      [ "Config_serial_port", "class_sky_traq_bin_1_1_config__serial__port.html", "class_sky_traq_bin_1_1_config__serial__port" ],
      [ "Config_NMEA_msg", "class_sky_traq_bin_1_1_config___n_m_e_a__msg.html", "class_sky_traq_bin_1_1_config___n_m_e_a__msg" ],
      [ "Config_msg_type", "class_sky_traq_bin_1_1_config__msg__type.html", "class_sky_traq_bin_1_1_config__msg__type" ],
      [ "Sw_img_download", "class_sky_traq_bin_1_1_sw__img__download.html", "class_sky_traq_bin_1_1_sw__img__download" ],
      [ "Config_sys_power_mode", "class_sky_traq_bin_1_1_config__sys__power__mode.html", "class_sky_traq_bin_1_1_config__sys__power__mode" ],
      [ "Config_sys_pos_rate", "class_sky_traq_bin_1_1_config__sys__pos__rate.html", "class_sky_traq_bin_1_1_config__sys__pos__rate" ],
      [ "Q_pos_update_rate", "class_sky_traq_bin_1_1_q__pos__update__rate.html", "class_sky_traq_bin_1_1_q__pos__update__rate" ],
      [ "Config_nav_data_msg_interval", "class_sky_traq_bin_1_1_config__nav__data__msg__interval.html", "class_sky_traq_bin_1_1_config__nav__data__msg__interval" ],
      [ "Get_almanac", "class_sky_traq_bin_1_1_get__almanac.html", "class_sky_traq_bin_1_1_get__almanac" ],
      [ "Config_bin_measurement_output_rates", "class_sky_traq_bin_1_1_config__bin__measurement__output__rates.html", "class_sky_traq_bin_1_1_config__bin__measurement__output__rates" ],
      [ "Q_power_mode", "class_sky_traq_bin_1_1_q__power__mode.html", "class_sky_traq_bin_1_1_q__power__mode" ],
      [ "Q_log_status", "class_sky_traq_bin_1_1_q__log__status.html", "class_sky_traq_bin_1_1_q__log__status" ],
      [ "Config_logging", "class_sky_traq_bin_1_1_config__logging.html", "class_sky_traq_bin_1_1_config__logging" ],
      [ "Clear_log", "class_sky_traq_bin_1_1_clear__log.html", "class_sky_traq_bin_1_1_clear__log" ],
      [ "Read_log", "class_sky_traq_bin_1_1_read__log.html", "class_sky_traq_bin_1_1_read__log" ],
      [ "Config_bin_measurement_data_output", "class_sky_traq_bin_1_1_config__bin__measurement__data__output.html", "class_sky_traq_bin_1_1_config__bin__measurement__data__output" ],
      [ "Q_bin_messurement_data_output_status", "class_sky_traq_bin_1_1_q__bin__messurement__data__output__status.html", "class_sky_traq_bin_1_1_q__bin__messurement__data__output__status" ],
      [ "Config_datum", "class_sky_traq_bin_1_1_config__datum.html", "class_sky_traq_bin_1_1_config__datum" ],
      [ "Config_DOP_mask", "class_sky_traq_bin_1_1_config___d_o_p__mask.html", "class_sky_traq_bin_1_1_config___d_o_p__mask" ],
      [ "Config_elevation_CNR_mask", "class_sky_traq_bin_1_1_config__elevation___c_n_r__mask.html", "class_sky_traq_bin_1_1_config__elevation___c_n_r__mask" ],
      [ "Q_datum", "class_sky_traq_bin_1_1_q__datum.html", "class_sky_traq_bin_1_1_q__datum" ],
      [ "Q_DOP_mask", "class_sky_traq_bin_1_1_q___d_o_p__mask.html", "class_sky_traq_bin_1_1_q___d_o_p__mask" ],
      [ "Q_elevation_CNR_mask", "class_sky_traq_bin_1_1_q__elevation___c_n_r__mask.html", "class_sky_traq_bin_1_1_q__elevation___c_n_r__mask" ],
      [ "Get_GPS_ephemeris", "class_sky_traq_bin_1_1_get___g_p_s__ephemeris.html", "class_sky_traq_bin_1_1_get___g_p_s__ephemeris" ],
      [ "Config_pos_pinning", "class_sky_traq_bin_1_1_config__pos__pinning.html", "class_sky_traq_bin_1_1_config__pos__pinning" ],
      [ "Q_pos_pinning", "class_sky_traq_bin_1_1_q__pos__pinning.html", "class_sky_traq_bin_1_1_q__pos__pinning" ],
      [ "Config_pos_pinning_params", "class_sky_traq_bin_1_1_config__pos__pinning__params.html", "class_sky_traq_bin_1_1_config__pos__pinning__params" ],
      [ "Set_GPS_ephemeris", "class_sky_traq_bin_1_1_set___g_p_s__ephemeris.html", "class_sky_traq_bin_1_1_set___g_p_s__ephemeris" ],
      [ "Q_1PPS_timing", "class_sky_traq_bin_1_1_q__1_p_p_s__timing.html", "class_sky_traq_bin_1_1_q__1_p_p_s__timing" ],
      [ "Config_1PPS_cable_delay", "class_sky_traq_bin_1_1_config__1_p_p_s__cable__delay.html", "class_sky_traq_bin_1_1_config__1_p_p_s__cable__delay" ],
      [ "Q_1PPS_cable_delay", "class_sky_traq_bin_1_1_q__1_p_p_s__cable__delay.html", "class_sky_traq_bin_1_1_q__1_p_p_s__cable__delay" ],
      [ "Config_NMEA_talker_ID", "class_sky_traq_bin_1_1_config___n_m_e_a__talker___i_d.html", "class_sky_traq_bin_1_1_config___n_m_e_a__talker___i_d" ],
      [ "Q_NMEA_talker_ID", "class_sky_traq_bin_1_1_q___n_m_e_a__talker___i_d.html", "class_sky_traq_bin_1_1_q___n_m_e_a__talker___i_d" ],
      [ "Config_1PPS_timing", "class_sky_traq_bin_1_1_config__1_p_p_s__timing.html", "class_sky_traq_bin_1_1_config__1_p_p_s__timing" ],
      [ "Get_Glonass_ephemeris", "class_sky_traq_bin_1_1_get___glonass__ephemeris.html", "class_sky_traq_bin_1_1_get___glonass__ephemeris" ],
      [ "Set_Glonass_ephemeris", "class_sky_traq_bin_1_1_set___glonass__ephemeris.html", "class_sky_traq_bin_1_1_set___glonass__ephemeris" ]
    ] ],
    [ "SkyTraqBin_inputs_with_subid.hh", "_sky_traq_bin__inputs__with__subid_8hh.html", [
      [ "Input_message_with_subid", "class_sky_traq_bin_1_1_input__message__with__subid.html", "class_sky_traq_bin_1_1_input__message__with__subid" ],
      [ "Config_SBAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html", "class_sky_traq_bin_1_1_config___s_b_a_s" ],
      [ "Q_SBAS_status", "class_sky_traq_bin_1_1_q___s_b_a_s__status.html", "class_sky_traq_bin_1_1_q___s_b_a_s__status" ],
      [ "Config_QZSS", "class_sky_traq_bin_1_1_config___q_z_s_s.html", "class_sky_traq_bin_1_1_config___q_z_s_s" ],
      [ "Q_QZSS_status", "class_sky_traq_bin_1_1_q___q_z_s_s__status.html", "class_sky_traq_bin_1_1_q___q_z_s_s__status" ],
      [ "Config_SAEE", "class_sky_traq_bin_1_1_config___s_a_e_e.html", "class_sky_traq_bin_1_1_config___s_a_e_e" ],
      [ "Q_SAEE_status", "class_sky_traq_bin_1_1_q___s_a_e_e__status.html", "class_sky_traq_bin_1_1_q___s_a_e_e__status" ],
      [ "Q_GNSS_boot_status", "class_sky_traq_bin_1_1_q___g_n_s_s__boot__status.html", "class_sky_traq_bin_1_1_q___g_n_s_s__boot__status" ],
      [ "Config_extended_NMEA_msg_interval", "class_sky_traq_bin_1_1_config__extended___n_m_e_a__msg__interval.html", "class_sky_traq_bin_1_1_config__extended___n_m_e_a__msg__interval" ],
      [ "Q_extended_NMEA_msg_interval", "class_sky_traq_bin_1_1_q__extended___n_m_e_a__msg__interval.html", "class_sky_traq_bin_1_1_q__extended___n_m_e_a__msg__interval" ],
      [ "Config_interference_detection", "class_sky_traq_bin_1_1_config__interference__detection.html", "class_sky_traq_bin_1_1_config__interference__detection" ],
      [ "Q_interference_detection_status", "class_sky_traq_bin_1_1_q__interference__detection__status.html", "class_sky_traq_bin_1_1_q__interference__detection__status" ],
      [ "Config_GPS_param_search_engine_num", "class_sky_traq_bin_1_1_config___g_p_s__param__search__engine__num.html", "class_sky_traq_bin_1_1_config___g_p_s__param__search__engine__num" ],
      [ "Q_GPS_param_search_engine_num", "class_sky_traq_bin_1_1_q___g_p_s__param__search__engine__num.html", "class_sky_traq_bin_1_1_q___g_p_s__param__search__engine__num" ],
      [ "Config_GNSS_nav_mode", "class_sky_traq_bin_1_1_config___g_n_s_s__nav__mode.html", "class_sky_traq_bin_1_1_config___g_n_s_s__nav__mode" ],
      [ "Q_GNSS_nav_mode", "class_sky_traq_bin_1_1_q___g_n_s_s__nav__mode.html", "class_sky_traq_bin_1_1_q___g_n_s_s__nav__mode" ],
      [ "Config_constellation_type", "class_sky_traq_bin_1_1_config__constellation__type.html", "class_sky_traq_bin_1_1_config__constellation__type" ],
      [ "Q_constellation_type", "class_sky_traq_bin_1_1_q__constellation__type.html", "class_sky_traq_bin_1_1_q__constellation__type" ],
      [ "Config_leap_seconds", "class_sky_traq_bin_1_1_config__leap__seconds.html", "class_sky_traq_bin_1_1_config__leap__seconds" ],
      [ "Q_GPS_time", "class_sky_traq_bin_1_1_q___g_p_s__time.html", "class_sky_traq_bin_1_1_q___g_p_s__time" ],
      [ "Config_GNSS_datum_index", "class_sky_traq_bin_1_1_config___g_n_s_s__datum__index.html", "class_sky_traq_bin_1_1_config___g_n_s_s__datum__index" ],
      [ "Q_GNSS_datum_index", "class_sky_traq_bin_1_1_q___g_n_s_s__datum__index.html", "class_sky_traq_bin_1_1_q___g_n_s_s__datum__index" ],
      [ "Config_1PPS_pulse_width", "class_sky_traq_bin_1_1_config__1_p_p_s__pulse__width.html", "class_sky_traq_bin_1_1_config__1_p_p_s__pulse__width" ],
      [ "Q_1PPS_pulse_width", "class_sky_traq_bin_1_1_q__1_p_p_s__pulse__width.html", "class_sky_traq_bin_1_1_q__1_p_p_s__pulse__width" ],
      [ "Config_1PPS_freq_output", "class_sky_traq_bin_1_1_config__1_p_p_s__freq__output.html", "class_sky_traq_bin_1_1_config__1_p_p_s__freq__output" ],
      [ "Q_1PPS_freq_output", "class_sky_traq_bin_1_1_q__1_p_p_s__freq__output.html", "class_sky_traq_bin_1_1_q__1_p_p_s__freq__output" ]
    ] ],
    [ "SkyTraqBin_outputs.cc", "_sky_traq_bin__outputs_8cc.html", null ],
    [ "SkyTraqBin_outputs.hh", "_sky_traq_bin__outputs_8hh.html", [
      [ "Sw_ver", "class_sky_traq_bin_1_1_sw__ver.html", "class_sky_traq_bin_1_1_sw__ver" ],
      [ "Sw_CRC", "class_sky_traq_bin_1_1_sw___c_r_c.html", "class_sky_traq_bin_1_1_sw___c_r_c" ],
      [ "Ack", "class_sky_traq_bin_1_1_ack.html", "class_sky_traq_bin_1_1_ack" ],
      [ "Nack", "class_sky_traq_bin_1_1_nack.html", "class_sky_traq_bin_1_1_nack" ],
      [ "Pos_update_rate", "class_sky_traq_bin_1_1_pos__update__rate.html", "class_sky_traq_bin_1_1_pos__update__rate" ],
      [ "GPS_almanac_data", "class_sky_traq_bin_1_1_g_p_s__almanac__data.html", "class_sky_traq_bin_1_1_g_p_s__almanac__data" ],
      [ "Bin_measurement_data_output_status", "class_sky_traq_bin_1_1_bin__measurement__data__output__status.html", "class_sky_traq_bin_1_1_bin__measurement__data__output__status" ],
      [ "Glonass_ephemeris_data", "class_sky_traq_bin_1_1_glonass__ephemeris__data.html", "class_sky_traq_bin_1_1_glonass__ephemeris__data" ],
      [ "NMEA_talker_ID", "class_sky_traq_bin_1_1_n_m_e_a__talker___i_d.html", "class_sky_traq_bin_1_1_n_m_e_a__talker___i_d" ],
      [ "Log_status_output", "class_sky_traq_bin_1_1_log__status__output.html", "class_sky_traq_bin_1_1_log__status__output" ],
      [ "Nav_data_msg", "class_sky_traq_bin_1_1_nav__data__msg.html", "class_sky_traq_bin_1_1_nav__data__msg" ],
      [ "GNSS_datum", "class_sky_traq_bin_1_1_g_n_s_s__datum.html", "class_sky_traq_bin_1_1_g_n_s_s__datum" ],
      [ "GNSS_DOP_mask", "class_sky_traq_bin_1_1_g_n_s_s___d_o_p__mask.html", "class_sky_traq_bin_1_1_g_n_s_s___d_o_p__mask" ],
      [ "GNSS_elevation_CNR_mask", "class_sky_traq_bin_1_1_g_n_s_s__elevation___c_n_r__mask.html", "class_sky_traq_bin_1_1_g_n_s_s__elevation___c_n_r__mask" ],
      [ "GPS_ephemeris_data", "class_sky_traq_bin_1_1_g_p_s__ephemeris__data.html", "class_sky_traq_bin_1_1_g_p_s__ephemeris__data" ],
      [ "GNSS_pos_pinning_status", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status" ],
      [ "GNSS_power_mode_status", "class_sky_traq_bin_1_1_g_n_s_s__power__mode__status.html", "class_sky_traq_bin_1_1_g_n_s_s__power__mode__status" ],
      [ "GNSS_1PPS_cable_delay", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__cable__delay.html", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__cable__delay" ],
      [ "GNSS_1PPS_timing", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing" ],
      [ "Measurement_time", "class_sky_traq_bin_1_1_measurement__time.html", "class_sky_traq_bin_1_1_measurement__time" ],
      [ "RawMeasurement", "struct_sky_traq_bin_1_1_raw_measurement.html", "struct_sky_traq_bin_1_1_raw_measurement" ],
      [ "Raw_measurements", "class_sky_traq_bin_1_1_raw__measurements.html", "class_sky_traq_bin_1_1_raw__measurements" ],
      [ "SvStatus", "struct_sky_traq_bin_1_1_sv_status.html", "struct_sky_traq_bin_1_1_sv_status" ],
      [ "SV_channel_status", "class_sky_traq_bin_1_1_s_v__channel__status.html", "class_sky_traq_bin_1_1_s_v__channel__status" ],
      [ "Rcv_state", "class_sky_traq_bin_1_1_rcv__state.html", "class_sky_traq_bin_1_1_rcv__state" ],
      [ "GPS_subframe_data", "class_sky_traq_bin_1_1_g_p_s__subframe__data.html", "class_sky_traq_bin_1_1_g_p_s__subframe__data" ],
      [ "Glonass_string_data", "class_sky_traq_bin_1_1_glonass__string__data.html", "class_sky_traq_bin_1_1_glonass__string__data" ],
      [ "Beidou2_subframe_data", "class_sky_traq_bin_1_1_beidou2__subframe__data.html", "class_sky_traq_bin_1_1_beidou2__subframe__data" ]
    ] ],
    [ "SkyTraqBin_outputs_with_subid.hh", "_sky_traq_bin__outputs__with__subid_8hh.html", [
      [ "Output_message_with_subid", "class_sky_traq_bin_1_1_output__message__with__subid.html", "class_sky_traq_bin_1_1_output__message__with__subid" ],
      [ "GNSS_SBAS_status", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status" ],
      [ "GNSS_QZSS_status", "class_sky_traq_bin_1_1_g_n_s_s___q_z_s_s__status.html", "class_sky_traq_bin_1_1_g_n_s_s___q_z_s_s__status" ],
      [ "GNSS_SAEE_status", "class_sky_traq_bin_1_1_g_n_s_s___s_a_e_e__status.html", "class_sky_traq_bin_1_1_g_n_s_s___s_a_e_e__status" ],
      [ "GNSS_boot_status", "class_sky_traq_bin_1_1_g_n_s_s__boot__status.html", "class_sky_traq_bin_1_1_g_n_s_s__boot__status" ],
      [ "GNSS_extended_NMEA_msg_interval", "class_sky_traq_bin_1_1_g_n_s_s__extended___n_m_e_a__msg__interval.html", "class_sky_traq_bin_1_1_g_n_s_s__extended___n_m_e_a__msg__interval" ],
      [ "GNSS_interference_detection_status", "class_sky_traq_bin_1_1_g_n_s_s__interference__detection__status.html", "class_sky_traq_bin_1_1_g_n_s_s__interference__detection__status" ],
      [ "GPS_param_search_engine_num", "class_sky_traq_bin_1_1_g_p_s__param__search__engine__num.html", "class_sky_traq_bin_1_1_g_p_s__param__search__engine__num" ],
      [ "GNSS_nav_mode", "class_sky_traq_bin_1_1_g_n_s_s__nav__mode.html", "class_sky_traq_bin_1_1_g_n_s_s__nav__mode" ],
      [ "GNSS_constellation_type", "class_sky_traq_bin_1_1_g_n_s_s__constellation__type.html", "class_sky_traq_bin_1_1_g_n_s_s__constellation__type" ],
      [ "GNSS_time", "class_sky_traq_bin_1_1_g_n_s_s__time.html", "class_sky_traq_bin_1_1_g_n_s_s__time" ],
      [ "GNSS_datum_index", "class_sky_traq_bin_1_1_g_n_s_s__datum__index.html", "class_sky_traq_bin_1_1_g_n_s_s__datum__index" ],
      [ "GNSS_1PPS_pulse_width", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__pulse__width.html", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__pulse__width" ],
      [ "GNSS_1PPS_freq_output", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__freq__output.html", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__freq__output" ],
      [ "Sensor_data", "class_sky_traq_bin_1_1_sensor__data.html", "class_sky_traq_bin_1_1_sensor__data" ]
    ] ],
    [ "UBX.cc", "_u_b_x_8cc.html", "_u_b_x_8cc" ],
    [ "UBX.hh", "_u_b_x_8hh.html", "_u_b_x_8hh" ],
    [ "UBX_ack.cc", "_u_b_x__ack_8cc.html", null ],
    [ "UBX_ack.hh", "_u_b_x__ack_8hh.html", [
      [ "Nak", "class_u_b_x_1_1_ack_1_1_nak.html", "class_u_b_x_1_1_ack_1_1_nak" ],
      [ "Ack", "class_u_b_x_1_1_ack_1_1_ack.html", "class_u_b_x_1_1_ack_1_1_ack" ]
    ] ],
    [ "UBX_cfg.cc", "_u_b_x__cfg_8cc.html", null ],
    [ "UBX_cfg.hh", "_u_b_x__cfg_8hh.html", "_u_b_x__cfg_8hh" ]
];