var class_sky_traq_bin_1_1_g_n_s_s__time =
[
    [ "GNSS_time", "class_sky_traq_bin_1_1_g_n_s_s__time.html#a1a66256990e2b3843f70b88234ed00a2", null ],
    [ "current_leap_seconds", "class_sky_traq_bin_1_1_g_n_s_s__time.html#a12602de093c072b34e65dc1a38bef05f", null ],
    [ "default_leap_seconds", "class_sky_traq_bin_1_1_g_n_s_s__time.html#a2b557e327bbc511406bfc3ba5472efd4", null ],
    [ "leap_seconds_valid", "class_sky_traq_bin_1_1_g_n_s_s__time.html#a8fa60760238d49abfc21bfa25162605f", null ],
    [ "subtime_in_week_raw", "class_sky_traq_bin_1_1_g_n_s_s__time.html#ad349048a874266c48545c9b9a3ec9214", null ],
    [ "time_in_week", "class_sky_traq_bin_1_1_g_n_s_s__time.html#a430f1ca726bcd0c3efc897f00ca00049", null ],
    [ "time_in_week_raw", "class_sky_traq_bin_1_1_g_n_s_s__time.html#a65f50463e1d3e6c1bab7536ac4dc3efb", null ],
    [ "time_in_week_valid", "class_sky_traq_bin_1_1_g_n_s_s__time.html#aee77d70be84e5629b39b1f1b18e00fc0", null ],
    [ "week_number", "class_sky_traq_bin_1_1_g_n_s_s__time.html#a62b1bbfe7f4cf7467737ce9b6b914616", null ],
    [ "week_number_valid", "class_sky_traq_bin_1_1_g_n_s_s__time.html#a3f7945be5ea9782182c9fc36d47826fc", null ]
];