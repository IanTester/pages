var class_n_m_e_a0183_1_1_z_d_a =
[
    [ "ptr", "class_n_m_e_a0183_1_1_z_d_a.html#a378a85b3b9e66aca06b2bf9c233567fa", null ],
    [ "ZDA", "class_n_m_e_a0183_1_1_z_d_a.html#aa7049d139763b727157f300fc903a1ad", null ],
    [ "TZ_hours", "class_n_m_e_a0183_1_1_z_d_a.html#a3280b1a8404dd880310603c4c1317a52", null ],
    [ "TZ_minutes", "class_n_m_e_a0183_1_1_z_d_a.html#a48f70bd483790181d648de0b0f89a36e", null ],
    [ "UTC_date", "class_n_m_e_a0183_1_1_z_d_a.html#a8d04d732a0cbe2339bebca7e4905da9b", null ],
    [ "UTC_datetime", "class_n_m_e_a0183_1_1_z_d_a.html#ac566f5745b649c282bbf94e759629a4c", null ],
    [ "UTC_time", "class_n_m_e_a0183_1_1_z_d_a.html#acded0c3365aa54d973c3cce40af538ca", null ]
];