var struct_sky_traq_bin_1_1_sv_status =
[
    [ "SvStatus", "struct_sky_traq_bin_1_1_sv_status.html#a4a312775bd107ea3f8be9d8f947cfadd", null ],
    [ "azimuth", "struct_sky_traq_bin_1_1_sv_status.html#ac7bf90892e91c015778549823416979f", null ],
    [ "chan_for_differential", "struct_sky_traq_bin_1_1_sv_status.html#a63b5df454372dd97c5d30372f45598fb", null ],
    [ "chan_for_normal", "struct_sky_traq_bin_1_1_sv_status.html#ae20dff55ee3708e1e96006a04e52e26c", null ],
    [ "chan_has_bit_sync", "struct_sky_traq_bin_1_1_sv_status.html#afa66ac674069b4a6b18a5bef64a7f6de", null ],
    [ "chan_has_ephemeris", "struct_sky_traq_bin_1_1_sv_status.html#a979214faa0041e2ad9f4400813846bf7", null ],
    [ "chan_has_frame_sync", "struct_sky_traq_bin_1_1_sv_status.html#a9b06b887a92c0f69e12a2f0cd4db3fbb", null ],
    [ "chan_has_pull_in", "struct_sky_traq_bin_1_1_sv_status.html#ac1d4310cbbf9968833311ab46e92b49c", null ],
    [ "channel_id", "struct_sky_traq_bin_1_1_sv_status.html#a90e9fa25be2565cbbe16c481c1c228e5", null ],
    [ "CN0", "struct_sky_traq_bin_1_1_sv_status.html#a3c3241c370fc14b29febadef39c0788a", null ],
    [ "elevation", "struct_sky_traq_bin_1_1_sv_status.html#ab5051e9da2f57878302b4cf8928941d7", null ],
    [ "PRN", "struct_sky_traq_bin_1_1_sv_status.html#af9d201b421aa274fce7941d750ae0e9e", null ],
    [ "sv_has_almanac", "struct_sky_traq_bin_1_1_sv_status.html#a036960cab7b0bda82ed4d09fcea8be6d", null ],
    [ "sv_has_ephemeris", "struct_sky_traq_bin_1_1_sv_status.html#a598af2a77dc61b96393f5985a7653923", null ],
    [ "sv_is_healthy", "struct_sky_traq_bin_1_1_sv_status.html#adc53495caa10eb5bdc4e2c5ddd30b0df", null ],
    [ "URA", "struct_sky_traq_bin_1_1_sv_status.html#a24cde94b4f6238d0a395115583e3fe26", null ]
];