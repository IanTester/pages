var class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status =
[
    [ "GNSS_SBAS_status", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#af9c2366b651d26b7e7094afa55a50a91", null ],
    [ "All_SBAS_enabled", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#ab2427fff53cb427d8c42aee81675e79d", null ],
    [ "correction", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#a18ea8fa29a16b7b4a6593f9c46cb11ad", null ],
    [ "EGNOS_enabled", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#a090e1fbdf1cbcf37b9ab3d1a8a3d822c", null ],
    [ "enabled", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#a5206f24b7ebb67abf99893d4cd51ac93", null ],
    [ "MSAS_enabled", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#a7f625794fe64434593bc23d460ea9814", null ],
    [ "num_channels", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#abc84eae96e73b88a11b55e312733fc2c", null ],
    [ "ranging", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#a95f94bb1f89ffa82553e156f91428ed5", null ],
    [ "ranging_URA_mask", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#a4977a48da505eacf8360e8e2188a886d", null ],
    [ "WAAS_enabled", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html#ae51a9dbd049a6d0c678e7d15ca475d3b", null ]
];