var class_sky_traq_bin_1_1_config___s_b_a_s =
[
    [ "Config_SBAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#aa1130fe3b3abbb6fc550022cdde79dd7", null ],
    [ "Config_SBAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a6210f5e0f94603a7521b1fda29e82794", null ],
    [ "All_SBAS_enabled", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a42e3ccb22b34c5bac44a6d1a64bd4c7f", null ],
    [ "correction", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a9f6f380339e57c3e672154e3049d5d3a", null ],
    [ "disable", "class_sky_traq_bin_1_1_config___s_b_a_s.html#ad765fe9db34b350f72aa8e14073600d9", null ],
    [ "disable_All_SBAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#ab1ac817be4c77cfe1eca73779133b717", null ],
    [ "disable_EGNOS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#ac7165aa3f47906278188af4df827b590", null ],
    [ "disable_MSAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#af3db1e8d66cf44e1649ca3bb2c386e7c", null ],
    [ "disable_WAAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#adc1232fa5f37426b5ca0caa5f1b96ff2", null ],
    [ "EGNOS_enabled", "class_sky_traq_bin_1_1_config___s_b_a_s.html#ae968a7dbe9324ac31b7eaf450f315946", null ],
    [ "enable", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a9f282a215225c14fba9229d765089654", null ],
    [ "enable_All_SBAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a51fa64d65942af76157bee3b5a72d4a7", null ],
    [ "enable_EGNOS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#aa82312fb1c495315f6db47adf512257c", null ],
    [ "enable_MSAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#aad175dec825930fc4660026f26a4e0e5", null ],
    [ "enable_WAAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a466cf219f67c6b13f185aa7216bdfeb6", null ],
    [ "enabled", "class_sky_traq_bin_1_1_config___s_b_a_s.html#ac347c3ecbb81fdf33fe6f47f3c9aa4f6", null ],
    [ "MSAS_enabled", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a45da3ddbdf07d382c15c23e3f93189f3", null ],
    [ "num_channels", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a2cd51b3e0115e4937161d2b66de764b1", null ],
    [ "ranging", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a8fd4cd60ff3cfb33fc5ec20ae27a33a3", null ],
    [ "ranging_URA_mask", "class_sky_traq_bin_1_1_config___s_b_a_s.html#ad37123adc15b0049e45cf9a9222a56f9", null ],
    [ "set_correction", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a2161a64166604b2699c3e553a0b84f51", null ],
    [ "set_num_channels", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a25a3091fae5be6cb60c3e17a8f18c527", null ],
    [ "set_ranging", "class_sky_traq_bin_1_1_config___s_b_a_s.html#afd8f3f36f2017cf683e3cd3b3175d2ba", null ],
    [ "set_ranging_URA_mask", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a3e6f289eb462550e36e5685b9e531923", null ],
    [ "set_update_type", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a1d3312c99a3ca51840c18a5d72a31b3d", null ],
    [ "unset_correction", "class_sky_traq_bin_1_1_config___s_b_a_s.html#a7f96362979725121abbdd445365c54fd", null ],
    [ "update_type", "class_sky_traq_bin_1_1_config___s_b_a_s.html#add2fe01a60deca2519121fe0f7ead6d5", null ],
    [ "WAAS_enabled", "class_sky_traq_bin_1_1_config___s_b_a_s.html#addb810812bef811e0b2900031ef29b88", null ]
];