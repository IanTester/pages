var class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing =
[
    [ "GNSS_1PPS_timing", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#a69662dacce7943485de423257ac97ce2", null ],
    [ "current_survey_length", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#a80baffd41d5b336ced81450f6b95c183", null ],
    [ "current_timing_mode", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#ab8ba4578e1614f036e6faf034636cf32", null ],
    [ "saved_altitude", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#a982551c35d26f399a846f7ae30a174e3", null ],
    [ "saved_lattitude", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#a0b2ec95210fb3f7e6c1abbb9bcd1f5ae", null ],
    [ "saved_longitude", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#aa67b106542509645b6f76b5bef9f0683", null ],
    [ "saved_standard_deviation", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#abef9cbffd99cb8e5c836d39faa838b36", null ],
    [ "saved_survey_length", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#ac59d7e3f8568264466b38ab07d0de9ee", null ],
    [ "saved_timing_mode", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html#a9993e2450a328b15b5476bb17e0dc16c", null ]
];