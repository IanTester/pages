var class_sky_traq_bin_1_1_sensor__data =
[
    [ "Sensor_data", "class_sky_traq_bin_1_1_sensor__data.html#aae400171016663473143466c6adacf4c", null ],
    [ "Gx", "class_sky_traq_bin_1_1_sensor__data.html#a3bd7c049be3ff25b6be25c70f270e5b8", null ],
    [ "Gy", "class_sky_traq_bin_1_1_sensor__data.html#aaf67affe5c35d2fa2153365e145f9b05", null ],
    [ "Gz", "class_sky_traq_bin_1_1_sensor__data.html#ab857a134880f5d2a7a3f4c68045953f5", null ],
    [ "Mx", "class_sky_traq_bin_1_1_sensor__data.html#aa607a27ca49d09ceb625b591fe4216fb", null ],
    [ "Mx_raw", "class_sky_traq_bin_1_1_sensor__data.html#a9e765825cd09b8e53ce511d7a244542b", null ],
    [ "My", "class_sky_traq_bin_1_1_sensor__data.html#aa5bb7d9e0ffd6b1b3c5906e22833d50d", null ],
    [ "My_raw", "class_sky_traq_bin_1_1_sensor__data.html#ac7a9c3bf952f5e34a874bb0f60eee516", null ],
    [ "Mz", "class_sky_traq_bin_1_1_sensor__data.html#af74864705f79697bb4e4919f4b9e705e", null ],
    [ "Mz_raw", "class_sky_traq_bin_1_1_sensor__data.html#a7d48c97b3c2a8a3b833127c16a61eb1e", null ],
    [ "pressure", "class_sky_traq_bin_1_1_sensor__data.html#adeea2eeeb9f8ff28629e9cad8e487206", null ],
    [ "temperature", "class_sky_traq_bin_1_1_sensor__data.html#a733f956bcfa4ccd04c5c3b3575c360c0", null ]
];