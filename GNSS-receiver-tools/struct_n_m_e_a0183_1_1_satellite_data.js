var struct_n_m_e_a0183_1_1_satellite_data =
[
    [ "ptr", "struct_n_m_e_a0183_1_1_satellite_data.html#a14db47322eea2285d4854353a6480864", null ],
    [ "SatelliteData", "struct_n_m_e_a0183_1_1_satellite_data.html#a29570db66dd0aa5dd4fa610430f51c43", null ],
    [ "azimuth", "struct_n_m_e_a0183_1_1_satellite_data.html#acde5ddbdeeeed2b422845955a773e89f", null ],
    [ "elevation", "struct_n_m_e_a0183_1_1_satellite_data.html#a4dea93e690df2b0cb755a33e07e32d3c", null ],
    [ "id", "struct_n_m_e_a0183_1_1_satellite_data.html#ac14614370f60dc8052efbe247faccfb9", null ],
    [ "snr", "struct_n_m_e_a0183_1_1_satellite_data.html#acd9851c68c6dc17855cc0b9ff41e66fe", null ]
];