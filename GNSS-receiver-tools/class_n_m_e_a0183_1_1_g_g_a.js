var class_n_m_e_a0183_1_1_g_g_a =
[
    [ "ptr", "class_n_m_e_a0183_1_1_g_g_a.html#addbd65b115f3306c8b2e146e42bd823e", null ],
    [ "GGA", "class_n_m_e_a0183_1_1_g_g_a.html#a948b0c8c13eb3bdea0b041a9f87be3f6", null ],
    [ "altitude", "class_n_m_e_a0183_1_1_g_g_a.html#ac651c8477b776a2372e9074b5fbc98c2", null ],
    [ "DGPS_station_id", "class_n_m_e_a0183_1_1_g_g_a.html#a1672281deb65f45263afd0d2ed469218", null ],
    [ "DGPS_update_age", "class_n_m_e_a0183_1_1_g_g_a.html#a572eab1ee24ac96622f2daeeab54eaea", null ],
    [ "fix_quality", "class_n_m_e_a0183_1_1_g_g_a.html#a7013c2f43dcda81b027e344ee15fcb83", null ],
    [ "GEOID_separation", "class_n_m_e_a0183_1_1_g_g_a.html#afbe469d946f2ebc4e832be23da4ad559", null ],
    [ "HDOP", "class_n_m_e_a0183_1_1_g_g_a.html#aa5e569f9dca30557ab484db7237d205e", null ],
    [ "lattitude", "class_n_m_e_a0183_1_1_g_g_a.html#a944e4765151a98805551ce375abf1326", null ],
    [ "longitude", "class_n_m_e_a0183_1_1_g_g_a.html#a78450d71e50a186c76c4046d6f9e37bf", null ],
    [ "num_sats_used", "class_n_m_e_a0183_1_1_g_g_a.html#afefdd9def2f14fdd8efee2f84b327cee", null ],
    [ "UTC_time", "class_n_m_e_a0183_1_1_g_g_a.html#ab92eca403cea7e025a114f6239de9065", null ]
];