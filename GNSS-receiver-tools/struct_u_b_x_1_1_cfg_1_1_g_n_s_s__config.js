var struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config =
[
    [ "GNSS_config", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html#afbbad6a115995a781387034c99d74e5b", null ],
    [ "enabled", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html#aec4e8b97c74d64663cd4c163ff70ff32", null ],
    [ "gnss_id", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html#a49ab50603a93ceba8f92f01779b81ec7", null ],
    [ "max_tracking_channels", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html#af4eccac61f7393401a697f6432bbb389", null ],
    [ "min_tracking_channels", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html#a3a7d1f4f54ff797f1b2376608d419848", null ],
    [ "signal_config_mask", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html#afb77f287db7cf78c8a4f07ff384f745f", null ]
];