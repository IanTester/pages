var class_n_m_e_a0183_1_1_g_l_l =
[
    [ "ptr", "class_n_m_e_a0183_1_1_g_l_l.html#a45427c46e54c74ee3e2a9af88c5c7ef8", null ],
    [ "GLL", "class_n_m_e_a0183_1_1_g_l_l.html#aa2b3d32f0a6d83576993982b1db6312c", null ],
    [ "lattitude", "class_n_m_e_a0183_1_1_g_l_l.html#a3b8711b7b7d38b0a4fbd53dc3bfeafa3", null ],
    [ "longitude", "class_n_m_e_a0183_1_1_g_l_l.html#ab049a97688278d54bdae1d988e2f392e", null ],
    [ "receiver_mode", "class_n_m_e_a0183_1_1_g_l_l.html#a32760a65a2b2d3b0719dc6c5efa5c378", null ],
    [ "UTC_time", "class_n_m_e_a0183_1_1_g_l_l.html#a0a67b479201e34ffc479236c3430742e", null ]
];