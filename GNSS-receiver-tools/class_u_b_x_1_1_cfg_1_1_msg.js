var class_u_b_x_1_1_cfg_1_1_msg =
[
    [ "Msg", "class_u_b_x_1_1_cfg_1_1_msg.html#a6553c0bb923278b72778905e784cc0ae", null ],
    [ "Msg", "class_u_b_x_1_1_cfg_1_1_msg.html#aa3dafe113bd66d225d4555fa2bca73e4", null ],
    [ "Msg", "class_u_b_x_1_1_cfg_1_1_msg.html#a531a16330fbb158d1d388864c5a65659", null ],
    [ "GETTER", "class_u_b_x_1_1_cfg_1_1_msg.html#a716d982d93ea8e2d294f253e4716b1d3", null ],
    [ "GETTER_SETTER", "class_u_b_x_1_1_cfg_1_1_msg.html#adff30673b0ebe84c0b9ff08176df1306", null ],
    [ "GETTER_SETTER", "class_u_b_x_1_1_cfg_1_1_msg.html#a1c5e265f20d72d7ef29b836cc1c53d31", null ],
    [ "message_name", "class_u_b_x_1_1_cfg_1_1_msg.html#af57f815db8c3ff2905984c8c68cc809d", null ],
    [ "rate", "class_u_b_x_1_1_cfg_1_1_msg.html#a5ece9ea973cebd4cced89e3532e1f9c6", null ],
    [ "rate", "class_u_b_x_1_1_cfg_1_1_msg.html#a76a5d7371903a4a31fcbb49279e8e383", null ],
    [ "set_rate", "class_u_b_x_1_1_cfg_1_1_msg.html#ae4c612979645c697704cda256e70b430", null ],
    [ "set_rate", "class_u_b_x_1_1_cfg_1_1_msg.html#ab35115bbec4d0d7611373d153ba59cd7", null ]
];