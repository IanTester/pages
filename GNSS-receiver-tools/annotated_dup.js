var annotated_dup =
[
    [ "GNSS", "namespace_g_n_s_s.html", [
      [ "Message", "class_g_n_s_s_1_1_message.html", "class_g_n_s_s_1_1_message" ],
      [ "InsufficientData", "class_g_n_s_s_1_1_insufficient_data.html", "class_g_n_s_s_1_1_insufficient_data" ],
      [ "InvalidMessage", "class_g_n_s_s_1_1_invalid_message.html", "class_g_n_s_s_1_1_invalid_message" ],
      [ "EndOfFile", "class_g_n_s_s_1_1_end_of_file.html", "class_g_n_s_s_1_1_end_of_file" ],
      [ "NotSendable", "class_g_n_s_s_1_1_not_sendable.html", "class_g_n_s_s_1_1_not_sendable" ],
      [ "Parser", "class_g_n_s_s_1_1_parser.html", "class_g_n_s_s_1_1_parser" ],
      [ "Listener", "class_g_n_s_s_1_1_listener.html", "class_g_n_s_s_1_1_listener" ],
      [ "Listener_SkyTraq", "class_g_n_s_s_1_1_listener___sky_traq.html", "class_g_n_s_s_1_1_listener___sky_traq" ],
      [ "Listener_Ublox", "class_g_n_s_s_1_1_listener___ublox.html", "class_g_n_s_s_1_1_listener___ublox" ],
      [ "Interface", "class_g_n_s_s_1_1_interface.html", "class_g_n_s_s_1_1_interface" ]
    ] ],
    [ "GPS", "namespace_g_p_s.html", [
      [ "Subframe", "class_g_p_s_1_1_subframe.html", "class_g_p_s_1_1_subframe" ],
      [ "Sat_clock_and_health", "class_g_p_s_1_1_sat__clock__and__health.html", "class_g_p_s_1_1_sat__clock__and__health" ],
      [ "Ephemeris1", "class_g_p_s_1_1_ephemeris1.html", "class_g_p_s_1_1_ephemeris1" ],
      [ "Ephemeris2", "class_g_p_s_1_1_ephemeris2.html", "class_g_p_s_1_1_ephemeris2" ],
      [ "Subframe_4_or_5", "class_g_p_s_1_1_subframe__4__or__5.html", "class_g_p_s_1_1_subframe__4__or__5" ],
      [ "Reserved_and_spare", "class_g_p_s_1_1_reserved__and__spare.html", "class_g_p_s_1_1_reserved__and__spare" ],
      [ "Almanac", "class_g_p_s_1_1_almanac.html", "class_g_p_s_1_1_almanac" ],
      [ "Special_message", "class_g_p_s_1_1_special__message.html", "class_g_p_s_1_1_special__message" ],
      [ "Ionosphere_UTC", "class_g_p_s_1_1_ionosphere___u_t_c.html", "class_g_p_s_1_1_ionosphere___u_t_c" ],
      [ "Sat_config", "class_g_p_s_1_1_sat__config.html", "class_g_p_s_1_1_sat__config" ],
      [ "Sat_health", "class_g_p_s_1_1_sat__health.html", "class_g_p_s_1_1_sat__health" ]
    ] ],
    [ "NMEA0183", "namespace_n_m_e_a0183.html", [
      [ "Skytraq", "namespace_n_m_e_a0183_1_1_skytraq.html", [
        [ "PPS", "class_n_m_e_a0183_1_1_skytraq_1_1_p_p_s.html", "class_n_m_e_a0183_1_1_skytraq_1_1_p_p_s" ],
        [ "Sensors", "class_n_m_e_a0183_1_1_skytraq_1_1_sensors.html", "class_n_m_e_a0183_1_1_skytraq_1_1_sensors" ]
      ] ],
      [ "InvalidSentence", "class_n_m_e_a0183_1_1_invalid_sentence.html", "class_n_m_e_a0183_1_1_invalid_sentence" ],
      [ "ChecksumMismatch", "class_n_m_e_a0183_1_1_checksum_mismatch.html", "class_n_m_e_a0183_1_1_checksum_mismatch" ],
      [ "UnknownSentenceType", "class_n_m_e_a0183_1_1_unknown_sentence_type.html", "class_n_m_e_a0183_1_1_unknown_sentence_type" ],
      [ "Sentence", "class_n_m_e_a0183_1_1_sentence.html", "class_n_m_e_a0183_1_1_sentence" ],
      [ "GGA", "class_n_m_e_a0183_1_1_g_g_a.html", "class_n_m_e_a0183_1_1_g_g_a" ],
      [ "GLL", "class_n_m_e_a0183_1_1_g_l_l.html", "class_n_m_e_a0183_1_1_g_l_l" ],
      [ "GSA", "class_n_m_e_a0183_1_1_g_s_a.html", "class_n_m_e_a0183_1_1_g_s_a" ],
      [ "SatelliteData", "struct_n_m_e_a0183_1_1_satellite_data.html", "struct_n_m_e_a0183_1_1_satellite_data" ],
      [ "GSV", "class_n_m_e_a0183_1_1_g_s_v.html", "class_n_m_e_a0183_1_1_g_s_v" ],
      [ "RMC", "class_n_m_e_a0183_1_1_r_m_c.html", "class_n_m_e_a0183_1_1_r_m_c" ],
      [ "VTG", "class_n_m_e_a0183_1_1_v_t_g.html", "class_n_m_e_a0183_1_1_v_t_g" ],
      [ "ZDA", "class_n_m_e_a0183_1_1_z_d_a.html", "class_n_m_e_a0183_1_1_z_d_a" ]
    ] ],
    [ "SkyTraqBin", "namespace_sky_traq_bin.html", [
      [ "ChecksumMismatch", "class_sky_traq_bin_1_1_checksum_mismatch.html", "class_sky_traq_bin_1_1_checksum_mismatch" ],
      [ "UnknownMessageID", "class_sky_traq_bin_1_1_unknown_message_i_d.html", "class_sky_traq_bin_1_1_unknown_message_i_d" ],
      [ "Message", "class_sky_traq_bin_1_1_message.html", "class_sky_traq_bin_1_1_message" ],
      [ "Output_message", "class_sky_traq_bin_1_1_output__message.html", "class_sky_traq_bin_1_1_output__message" ],
      [ "Input_message", "class_sky_traq_bin_1_1_input__message.html", "class_sky_traq_bin_1_1_input__message" ],
      [ "with_subid", "class_sky_traq_bin_1_1with__subid.html", "class_sky_traq_bin_1_1with__subid" ],
      [ "with_response", "class_sky_traq_bin_1_1with__response.html", "class_sky_traq_bin_1_1with__response" ],
      [ "Restart_sys", "class_sky_traq_bin_1_1_restart__sys.html", "class_sky_traq_bin_1_1_restart__sys" ],
      [ "Q_sw_ver", "class_sky_traq_bin_1_1_q__sw__ver.html", "class_sky_traq_bin_1_1_q__sw__ver" ],
      [ "Q_sw_CRC", "class_sky_traq_bin_1_1_q__sw___c_r_c.html", "class_sky_traq_bin_1_1_q__sw___c_r_c" ],
      [ "Set_factory_defaults", "class_sky_traq_bin_1_1_set__factory__defaults.html", "class_sky_traq_bin_1_1_set__factory__defaults" ],
      [ "Config_serial_port", "class_sky_traq_bin_1_1_config__serial__port.html", "class_sky_traq_bin_1_1_config__serial__port" ],
      [ "Config_NMEA_msg", "class_sky_traq_bin_1_1_config___n_m_e_a__msg.html", "class_sky_traq_bin_1_1_config___n_m_e_a__msg" ],
      [ "Config_msg_type", "class_sky_traq_bin_1_1_config__msg__type.html", "class_sky_traq_bin_1_1_config__msg__type" ],
      [ "Sw_img_download", "class_sky_traq_bin_1_1_sw__img__download.html", "class_sky_traq_bin_1_1_sw__img__download" ],
      [ "Config_sys_power_mode", "class_sky_traq_bin_1_1_config__sys__power__mode.html", "class_sky_traq_bin_1_1_config__sys__power__mode" ],
      [ "Config_sys_pos_rate", "class_sky_traq_bin_1_1_config__sys__pos__rate.html", "class_sky_traq_bin_1_1_config__sys__pos__rate" ],
      [ "Q_pos_update_rate", "class_sky_traq_bin_1_1_q__pos__update__rate.html", "class_sky_traq_bin_1_1_q__pos__update__rate" ],
      [ "Config_nav_data_msg_interval", "class_sky_traq_bin_1_1_config__nav__data__msg__interval.html", "class_sky_traq_bin_1_1_config__nav__data__msg__interval" ],
      [ "Get_almanac", "class_sky_traq_bin_1_1_get__almanac.html", "class_sky_traq_bin_1_1_get__almanac" ],
      [ "Config_bin_measurement_output_rates", "class_sky_traq_bin_1_1_config__bin__measurement__output__rates.html", "class_sky_traq_bin_1_1_config__bin__measurement__output__rates" ],
      [ "Q_power_mode", "class_sky_traq_bin_1_1_q__power__mode.html", "class_sky_traq_bin_1_1_q__power__mode" ],
      [ "Q_log_status", "class_sky_traq_bin_1_1_q__log__status.html", "class_sky_traq_bin_1_1_q__log__status" ],
      [ "Config_logging", "class_sky_traq_bin_1_1_config__logging.html", "class_sky_traq_bin_1_1_config__logging" ],
      [ "Clear_log", "class_sky_traq_bin_1_1_clear__log.html", "class_sky_traq_bin_1_1_clear__log" ],
      [ "Read_log", "class_sky_traq_bin_1_1_read__log.html", "class_sky_traq_bin_1_1_read__log" ],
      [ "Config_bin_measurement_data_output", "class_sky_traq_bin_1_1_config__bin__measurement__data__output.html", "class_sky_traq_bin_1_1_config__bin__measurement__data__output" ],
      [ "Q_bin_messurement_data_output_status", "class_sky_traq_bin_1_1_q__bin__messurement__data__output__status.html", "class_sky_traq_bin_1_1_q__bin__messurement__data__output__status" ],
      [ "Config_datum", "class_sky_traq_bin_1_1_config__datum.html", "class_sky_traq_bin_1_1_config__datum" ],
      [ "Config_DOP_mask", "class_sky_traq_bin_1_1_config___d_o_p__mask.html", "class_sky_traq_bin_1_1_config___d_o_p__mask" ],
      [ "Config_elevation_CNR_mask", "class_sky_traq_bin_1_1_config__elevation___c_n_r__mask.html", "class_sky_traq_bin_1_1_config__elevation___c_n_r__mask" ],
      [ "Q_datum", "class_sky_traq_bin_1_1_q__datum.html", "class_sky_traq_bin_1_1_q__datum" ],
      [ "Q_DOP_mask", "class_sky_traq_bin_1_1_q___d_o_p__mask.html", "class_sky_traq_bin_1_1_q___d_o_p__mask" ],
      [ "Q_elevation_CNR_mask", "class_sky_traq_bin_1_1_q__elevation___c_n_r__mask.html", "class_sky_traq_bin_1_1_q__elevation___c_n_r__mask" ],
      [ "Get_GPS_ephemeris", "class_sky_traq_bin_1_1_get___g_p_s__ephemeris.html", "class_sky_traq_bin_1_1_get___g_p_s__ephemeris" ],
      [ "Config_pos_pinning", "class_sky_traq_bin_1_1_config__pos__pinning.html", "class_sky_traq_bin_1_1_config__pos__pinning" ],
      [ "Q_pos_pinning", "class_sky_traq_bin_1_1_q__pos__pinning.html", "class_sky_traq_bin_1_1_q__pos__pinning" ],
      [ "Config_pos_pinning_params", "class_sky_traq_bin_1_1_config__pos__pinning__params.html", "class_sky_traq_bin_1_1_config__pos__pinning__params" ],
      [ "Set_GPS_ephemeris", "class_sky_traq_bin_1_1_set___g_p_s__ephemeris.html", "class_sky_traq_bin_1_1_set___g_p_s__ephemeris" ],
      [ "Q_1PPS_timing", "class_sky_traq_bin_1_1_q__1_p_p_s__timing.html", "class_sky_traq_bin_1_1_q__1_p_p_s__timing" ],
      [ "Config_1PPS_cable_delay", "class_sky_traq_bin_1_1_config__1_p_p_s__cable__delay.html", "class_sky_traq_bin_1_1_config__1_p_p_s__cable__delay" ],
      [ "Q_1PPS_cable_delay", "class_sky_traq_bin_1_1_q__1_p_p_s__cable__delay.html", "class_sky_traq_bin_1_1_q__1_p_p_s__cable__delay" ],
      [ "Config_NMEA_talker_ID", "class_sky_traq_bin_1_1_config___n_m_e_a__talker___i_d.html", "class_sky_traq_bin_1_1_config___n_m_e_a__talker___i_d" ],
      [ "Q_NMEA_talker_ID", "class_sky_traq_bin_1_1_q___n_m_e_a__talker___i_d.html", "class_sky_traq_bin_1_1_q___n_m_e_a__talker___i_d" ],
      [ "Config_1PPS_timing", "class_sky_traq_bin_1_1_config__1_p_p_s__timing.html", "class_sky_traq_bin_1_1_config__1_p_p_s__timing" ],
      [ "Get_Glonass_ephemeris", "class_sky_traq_bin_1_1_get___glonass__ephemeris.html", "class_sky_traq_bin_1_1_get___glonass__ephemeris" ],
      [ "Set_Glonass_ephemeris", "class_sky_traq_bin_1_1_set___glonass__ephemeris.html", "class_sky_traq_bin_1_1_set___glonass__ephemeris" ],
      [ "Input_message_with_subid", "class_sky_traq_bin_1_1_input__message__with__subid.html", "class_sky_traq_bin_1_1_input__message__with__subid" ],
      [ "Config_SBAS", "class_sky_traq_bin_1_1_config___s_b_a_s.html", "class_sky_traq_bin_1_1_config___s_b_a_s" ],
      [ "Q_SBAS_status", "class_sky_traq_bin_1_1_q___s_b_a_s__status.html", "class_sky_traq_bin_1_1_q___s_b_a_s__status" ],
      [ "Config_QZSS", "class_sky_traq_bin_1_1_config___q_z_s_s.html", "class_sky_traq_bin_1_1_config___q_z_s_s" ],
      [ "Q_QZSS_status", "class_sky_traq_bin_1_1_q___q_z_s_s__status.html", "class_sky_traq_bin_1_1_q___q_z_s_s__status" ],
      [ "Config_SAEE", "class_sky_traq_bin_1_1_config___s_a_e_e.html", "class_sky_traq_bin_1_1_config___s_a_e_e" ],
      [ "Q_SAEE_status", "class_sky_traq_bin_1_1_q___s_a_e_e__status.html", "class_sky_traq_bin_1_1_q___s_a_e_e__status" ],
      [ "Q_GNSS_boot_status", "class_sky_traq_bin_1_1_q___g_n_s_s__boot__status.html", "class_sky_traq_bin_1_1_q___g_n_s_s__boot__status" ],
      [ "Config_extended_NMEA_msg_interval", "class_sky_traq_bin_1_1_config__extended___n_m_e_a__msg__interval.html", "class_sky_traq_bin_1_1_config__extended___n_m_e_a__msg__interval" ],
      [ "Q_extended_NMEA_msg_interval", "class_sky_traq_bin_1_1_q__extended___n_m_e_a__msg__interval.html", "class_sky_traq_bin_1_1_q__extended___n_m_e_a__msg__interval" ],
      [ "Config_interference_detection", "class_sky_traq_bin_1_1_config__interference__detection.html", "class_sky_traq_bin_1_1_config__interference__detection" ],
      [ "Q_interference_detection_status", "class_sky_traq_bin_1_1_q__interference__detection__status.html", "class_sky_traq_bin_1_1_q__interference__detection__status" ],
      [ "Config_GPS_param_search_engine_num", "class_sky_traq_bin_1_1_config___g_p_s__param__search__engine__num.html", "class_sky_traq_bin_1_1_config___g_p_s__param__search__engine__num" ],
      [ "Q_GPS_param_search_engine_num", "class_sky_traq_bin_1_1_q___g_p_s__param__search__engine__num.html", "class_sky_traq_bin_1_1_q___g_p_s__param__search__engine__num" ],
      [ "Config_GNSS_nav_mode", "class_sky_traq_bin_1_1_config___g_n_s_s__nav__mode.html", "class_sky_traq_bin_1_1_config___g_n_s_s__nav__mode" ],
      [ "Q_GNSS_nav_mode", "class_sky_traq_bin_1_1_q___g_n_s_s__nav__mode.html", "class_sky_traq_bin_1_1_q___g_n_s_s__nav__mode" ],
      [ "Config_constellation_type", "class_sky_traq_bin_1_1_config__constellation__type.html", "class_sky_traq_bin_1_1_config__constellation__type" ],
      [ "Q_constellation_type", "class_sky_traq_bin_1_1_q__constellation__type.html", "class_sky_traq_bin_1_1_q__constellation__type" ],
      [ "Config_leap_seconds", "class_sky_traq_bin_1_1_config__leap__seconds.html", "class_sky_traq_bin_1_1_config__leap__seconds" ],
      [ "Q_GPS_time", "class_sky_traq_bin_1_1_q___g_p_s__time.html", "class_sky_traq_bin_1_1_q___g_p_s__time" ],
      [ "Config_GNSS_datum_index", "class_sky_traq_bin_1_1_config___g_n_s_s__datum__index.html", "class_sky_traq_bin_1_1_config___g_n_s_s__datum__index" ],
      [ "Q_GNSS_datum_index", "class_sky_traq_bin_1_1_q___g_n_s_s__datum__index.html", "class_sky_traq_bin_1_1_q___g_n_s_s__datum__index" ],
      [ "Config_1PPS_pulse_width", "class_sky_traq_bin_1_1_config__1_p_p_s__pulse__width.html", "class_sky_traq_bin_1_1_config__1_p_p_s__pulse__width" ],
      [ "Q_1PPS_pulse_width", "class_sky_traq_bin_1_1_q__1_p_p_s__pulse__width.html", "class_sky_traq_bin_1_1_q__1_p_p_s__pulse__width" ],
      [ "Config_1PPS_freq_output", "class_sky_traq_bin_1_1_config__1_p_p_s__freq__output.html", "class_sky_traq_bin_1_1_config__1_p_p_s__freq__output" ],
      [ "Q_1PPS_freq_output", "class_sky_traq_bin_1_1_q__1_p_p_s__freq__output.html", "class_sky_traq_bin_1_1_q__1_p_p_s__freq__output" ],
      [ "Sw_ver", "class_sky_traq_bin_1_1_sw__ver.html", "class_sky_traq_bin_1_1_sw__ver" ],
      [ "Sw_CRC", "class_sky_traq_bin_1_1_sw___c_r_c.html", "class_sky_traq_bin_1_1_sw___c_r_c" ],
      [ "Ack", "class_sky_traq_bin_1_1_ack.html", "class_sky_traq_bin_1_1_ack" ],
      [ "Nack", "class_sky_traq_bin_1_1_nack.html", "class_sky_traq_bin_1_1_nack" ],
      [ "Pos_update_rate", "class_sky_traq_bin_1_1_pos__update__rate.html", "class_sky_traq_bin_1_1_pos__update__rate" ],
      [ "GPS_almanac_data", "class_sky_traq_bin_1_1_g_p_s__almanac__data.html", "class_sky_traq_bin_1_1_g_p_s__almanac__data" ],
      [ "Bin_measurement_data_output_status", "class_sky_traq_bin_1_1_bin__measurement__data__output__status.html", "class_sky_traq_bin_1_1_bin__measurement__data__output__status" ],
      [ "Glonass_ephemeris_data", "class_sky_traq_bin_1_1_glonass__ephemeris__data.html", "class_sky_traq_bin_1_1_glonass__ephemeris__data" ],
      [ "NMEA_talker_ID", "class_sky_traq_bin_1_1_n_m_e_a__talker___i_d.html", "class_sky_traq_bin_1_1_n_m_e_a__talker___i_d" ],
      [ "Log_status_output", "class_sky_traq_bin_1_1_log__status__output.html", "class_sky_traq_bin_1_1_log__status__output" ],
      [ "Nav_data_msg", "class_sky_traq_bin_1_1_nav__data__msg.html", "class_sky_traq_bin_1_1_nav__data__msg" ],
      [ "GNSS_datum", "class_sky_traq_bin_1_1_g_n_s_s__datum.html", "class_sky_traq_bin_1_1_g_n_s_s__datum" ],
      [ "GNSS_DOP_mask", "class_sky_traq_bin_1_1_g_n_s_s___d_o_p__mask.html", "class_sky_traq_bin_1_1_g_n_s_s___d_o_p__mask" ],
      [ "GNSS_elevation_CNR_mask", "class_sky_traq_bin_1_1_g_n_s_s__elevation___c_n_r__mask.html", "class_sky_traq_bin_1_1_g_n_s_s__elevation___c_n_r__mask" ],
      [ "GPS_ephemeris_data", "class_sky_traq_bin_1_1_g_p_s__ephemeris__data.html", "class_sky_traq_bin_1_1_g_p_s__ephemeris__data" ],
      [ "GNSS_pos_pinning_status", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status.html", "class_sky_traq_bin_1_1_g_n_s_s__pos__pinning__status" ],
      [ "GNSS_power_mode_status", "class_sky_traq_bin_1_1_g_n_s_s__power__mode__status.html", "class_sky_traq_bin_1_1_g_n_s_s__power__mode__status" ],
      [ "GNSS_1PPS_cable_delay", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__cable__delay.html", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__cable__delay" ],
      [ "GNSS_1PPS_timing", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing.html", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__timing" ],
      [ "Measurement_time", "class_sky_traq_bin_1_1_measurement__time.html", "class_sky_traq_bin_1_1_measurement__time" ],
      [ "RawMeasurement", "struct_sky_traq_bin_1_1_raw_measurement.html", "struct_sky_traq_bin_1_1_raw_measurement" ],
      [ "Raw_measurements", "class_sky_traq_bin_1_1_raw__measurements.html", "class_sky_traq_bin_1_1_raw__measurements" ],
      [ "SvStatus", "struct_sky_traq_bin_1_1_sv_status.html", "struct_sky_traq_bin_1_1_sv_status" ],
      [ "SV_channel_status", "class_sky_traq_bin_1_1_s_v__channel__status.html", "class_sky_traq_bin_1_1_s_v__channel__status" ],
      [ "Rcv_state", "class_sky_traq_bin_1_1_rcv__state.html", "class_sky_traq_bin_1_1_rcv__state" ],
      [ "GPS_subframe_data", "class_sky_traq_bin_1_1_g_p_s__subframe__data.html", "class_sky_traq_bin_1_1_g_p_s__subframe__data" ],
      [ "Glonass_string_data", "class_sky_traq_bin_1_1_glonass__string__data.html", "class_sky_traq_bin_1_1_glonass__string__data" ],
      [ "Beidou2_subframe_data", "class_sky_traq_bin_1_1_beidou2__subframe__data.html", "class_sky_traq_bin_1_1_beidou2__subframe__data" ],
      [ "Output_message_with_subid", "class_sky_traq_bin_1_1_output__message__with__subid.html", "class_sky_traq_bin_1_1_output__message__with__subid" ],
      [ "GNSS_SBAS_status", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status.html", "class_sky_traq_bin_1_1_g_n_s_s___s_b_a_s__status" ],
      [ "GNSS_QZSS_status", "class_sky_traq_bin_1_1_g_n_s_s___q_z_s_s__status.html", "class_sky_traq_bin_1_1_g_n_s_s___q_z_s_s__status" ],
      [ "GNSS_SAEE_status", "class_sky_traq_bin_1_1_g_n_s_s___s_a_e_e__status.html", "class_sky_traq_bin_1_1_g_n_s_s___s_a_e_e__status" ],
      [ "GNSS_boot_status", "class_sky_traq_bin_1_1_g_n_s_s__boot__status.html", "class_sky_traq_bin_1_1_g_n_s_s__boot__status" ],
      [ "GNSS_extended_NMEA_msg_interval", "class_sky_traq_bin_1_1_g_n_s_s__extended___n_m_e_a__msg__interval.html", "class_sky_traq_bin_1_1_g_n_s_s__extended___n_m_e_a__msg__interval" ],
      [ "GNSS_interference_detection_status", "class_sky_traq_bin_1_1_g_n_s_s__interference__detection__status.html", "class_sky_traq_bin_1_1_g_n_s_s__interference__detection__status" ],
      [ "GPS_param_search_engine_num", "class_sky_traq_bin_1_1_g_p_s__param__search__engine__num.html", "class_sky_traq_bin_1_1_g_p_s__param__search__engine__num" ],
      [ "GNSS_nav_mode", "class_sky_traq_bin_1_1_g_n_s_s__nav__mode.html", "class_sky_traq_bin_1_1_g_n_s_s__nav__mode" ],
      [ "GNSS_constellation_type", "class_sky_traq_bin_1_1_g_n_s_s__constellation__type.html", "class_sky_traq_bin_1_1_g_n_s_s__constellation__type" ],
      [ "GNSS_time", "class_sky_traq_bin_1_1_g_n_s_s__time.html", "class_sky_traq_bin_1_1_g_n_s_s__time" ],
      [ "GNSS_datum_index", "class_sky_traq_bin_1_1_g_n_s_s__datum__index.html", "class_sky_traq_bin_1_1_g_n_s_s__datum__index" ],
      [ "GNSS_1PPS_pulse_width", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__pulse__width.html", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__pulse__width" ],
      [ "GNSS_1PPS_freq_output", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__freq__output.html", "class_sky_traq_bin_1_1_g_n_s_s__1_p_p_s__freq__output" ],
      [ "Sensor_data", "class_sky_traq_bin_1_1_sensor__data.html", "class_sky_traq_bin_1_1_sensor__data" ]
    ] ],
    [ "UBX", "namespace_u_b_x.html", [
      [ "Ack", "namespace_u_b_x_1_1_ack.html", [
        [ "Nak", "class_u_b_x_1_1_ack_1_1_nak.html", "class_u_b_x_1_1_ack_1_1_nak" ],
        [ "Ack", "class_u_b_x_1_1_ack_1_1_ack.html", "class_u_b_x_1_1_ack_1_1_ack" ]
      ] ],
      [ "Cfg", "namespace_u_b_x_1_1_cfg.html", [
        [ "Q_prt", "class_u_b_x_1_1_cfg_1_1_q__prt.html", "class_u_b_x_1_1_cfg_1_1_q__prt" ],
        [ "Prt", "class_u_b_x_1_1_cfg_1_1_prt.html", "class_u_b_x_1_1_cfg_1_1_prt" ],
        [ "Q_msg", "class_u_b_x_1_1_cfg_1_1_q__msg.html", "class_u_b_x_1_1_cfg_1_1_q__msg" ],
        [ "Msg", "class_u_b_x_1_1_cfg_1_1_msg.html", "class_u_b_x_1_1_cfg_1_1_msg" ],
        [ "Q_GNSS", "class_u_b_x_1_1_cfg_1_1_q___g_n_s_s.html", "class_u_b_x_1_1_cfg_1_1_q___g_n_s_s" ],
        [ "GNSS_config", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config.html", "struct_u_b_x_1_1_cfg_1_1_g_n_s_s__config" ],
        [ "GNSS", "class_u_b_x_1_1_cfg_1_1_g_n_s_s.html", "class_u_b_x_1_1_cfg_1_1_g_n_s_s" ]
      ] ],
      [ "ChecksumMismatch", "class_u_b_x_1_1_checksum_mismatch.html", "class_u_b_x_1_1_checksum_mismatch" ],
      [ "UnknownMessageID", "class_u_b_x_1_1_unknown_message_i_d.html", "class_u_b_x_1_1_unknown_message_i_d" ],
      [ "Message", "class_u_b_x_1_1_message.html", "class_u_b_x_1_1_message" ],
      [ "Output_message", "class_u_b_x_1_1_output__message.html", "class_u_b_x_1_1_output__message" ],
      [ "Input_message", "class_u_b_x_1_1_input__message.html", "class_u_b_x_1_1_input__message" ],
      [ "Input_Output_message", "class_u_b_x_1_1_input___output__message.html", "class_u_b_x_1_1_input___output__message" ],
      [ "checksum_t", "struct_u_b_x_1_1checksum__t.html", "struct_u_b_x_1_1checksum__t" ]
    ] ],
    [ "bitstream", "classbitstream.html", "classbitstream" ]
];